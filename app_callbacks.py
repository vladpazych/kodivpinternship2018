import numpy as np
import pandas as pd
import dash
import plotly.graph_objs as go
import dash_core_components as dcc
import dash_html_components as html
from lib import generate_color, generate_table
import app_style


def create(app: dash.Dash):
    df_cfc = pd.read_csv('./data/processed/correlation.csv')
    df_usg = pd.read_csv('./data/processed/usage.csv')
    df_str = pd.read_csv('./data/processed/starts.csv')
    df_usg['DateTime'] = pd.to_datetime(df_usg['DateTime'])
    df_str['DateTime'] = pd.to_datetime(df_str['DateTime'])

    @app.callback(
        dash.dependencies.Output('hour-slider-container', 'children'),
        [dash.dependencies.Input('time-range-selector', 'value')])
    def update_figure(selected_time_range):
        result = []
        if selected_time_range == 'EnableTimeRange':
            result = [
                dcc.RangeSlider(
                    id='hour-range-slider',
                    count=2,
                    min=0,
                    max=23,
                    step=1,
                    value=[12, 14],
                    marks={i: '{}'.format(i) if i == 1 else str(i) for i in range(0, 24)},
                )
            ]
        else:
            result = [
                dcc.Slider(
                    id='hour-range-slider',
                    min=0,
                    max=23,
                    step=1,
                    value=12,
                    included=False,
                    marks={i: '{}'.format(i) if i == 1 else str(i) for i in range(0, 24)},
                )
            ]

        return result

    @app.callback(
        dash.dependencies.Output('application-scatter', 'figure'),
        [dash.dependencies.Input('hour-range-slider', 'value'), ])
    def update_application_scatter(selected_time_range):
        if type(selected_time_range) is not list:
            selected_time_range = [selected_time_range]

        start_hour = selected_time_range[0]
        end_hour = selected_time_range[0] if len(
            selected_time_range) == 1 else selected_time_range[1]

        app_names = df_cfc.columns[1:]
        df_usg_current = df_usg.loc[(df_usg['DateTime'].dt.hour >= start_hour) &
                                    (df_usg['DateTime'].dt.hour <= end_hour)]
        grap_data = []

        for app_name in app_names:
            app_color = app_style.app_colors[app_name]
            grap_data.append(
                go.Scatter(
                    x=df_usg_current[app_name],
                    y=df_usg_current['Revenue'],
                    name=app_name,
                    mode='markers',
                    hoverinfo='name+x+y',
                    hoverlabel=dict(font=dict(size=14), namelength=-1),
                    marker=go.Marker(
                        color=app_color
                    )
                )
            )

        figure = go.Figure(
            data=grap_data,
            layout=go.Layout(
                xaxis=dict(
                    title='Time spent in apps per session (man-minutes)',
                    type='linear',
                    autorange=True,
                ),
                yaxis=dict(
                    title='Revenue per session',
                    type='linear',
                    autorange=True,
                    side='left'
                ),
                showlegend=False,
                legend=go.Legend(
                    x=0,
                    y=1.0
                ),
                hovermode='closest',
                margin=go.Margin(l=40, r=0, t=40, b=30)
            )
        )

        return figure

    @app.callback(
        dash.dependencies.Output('correlation-table', 'children'),
        [dash.dependencies.Input('hour-range-slider', 'value')])
    def update_table(selected_time_range):
        if type(selected_time_range) is not list:
            selected_time_range = [selected_time_range]

        start_hour = selected_time_range[0]
        end_hour = selected_time_range[0] if len(
            selected_time_range) == 1 else selected_time_range[1]

        df_corcol = start_hour
        df_cfc_local = df_cfc.T
        df_cfc_local['Application Name'] = df_cfc_local.index
        df_cfc_local = df_cfc_local.drop(labels=['Hour'], axis=0)

        df_cfc_local = pd.DataFrame(df_cfc_local[['Application Name', start_hour, end_hour]])
        df_cfc_local['Correlation Coefficient'] = df_cfc_local[[start_hour, end_hour]].mean(axis=1)
        df_cfc_local = df_cfc_local.drop(columns=[start_hour, end_hour])
        df_cfc_local['Correlation Coefficient'] = df_cfc_local['Correlation Coefficient'].round(
            decimals=3)
        df_cfc_local = df_cfc_local.sort_values(by='Correlation Coefficient', ascending=False)

        df_cfc_local['Avarage time spent (man-minutes)'] = 0
        df_cfc_local['Avarage number of app openings'] = 0
        app_names = df_cfc.columns[1:]

        for app_name in app_names:
            df_cfc_local.loc[app_name, 'Avarage time spent (man-minutes)'] = round(df_usg.loc[
                (df_usg['DateTime'].dt.hour >= start_hour) &
                (df_usg['DateTime'].dt.hour <= end_hour), app_name].mean(), 2)
            df_cfc_local.loc[app_name, 'Avarage number of app openings'] = round(df_str.loc[
                (df_str['DateTime'].dt.hour >= start_hour) &
                (df_str['DateTime'].dt.hour <= end_hour), app_name].mean(), 2)

        avarage_revenue = df_usg.loc[(df_usg['DateTime'].dt.hour >= start_hour) & (
            df_usg['DateTime'].dt.hour <= end_hour), 'Revenue'].mean()
        avarage_revenue = round(avarage_revenue, 2)
        number_of_sessions = df_usg.loc[(df_usg['DateTime'].dt.hour >= start_hour) & (
            df_usg['DateTime'].dt.hour <= end_hour)].count().mean()

        rows = df_cfc_local.to_dict('records')

        tr_list = []

        for i in range(min(len(df_cfc_local), 10)):
            td_list = []
            app_name = df_cfc_local.iloc[i]['Application Name']
            app_color = app_style.app_colors[app_name]
            for col in df_cfc_local.columns:
                td_list.append(html.Td(df_cfc_local.iloc[i][col], className='td', style={'color': app_color}))
            tr_list.append(html.Tr(td_list, className='tr'))

        return html.Div([
            html.Div([
                html.Table([
                    html.Tr([
                        html.Td(['Avarage Revenue'], style={'fontWeight': 'bold'}),
                        html.Td([str(avarage_revenue)])
                    ]),
                    html.Tr([
                        html.Td(['Number of recorded sessions'], style={'fontWeight': 'bold'}),
                        html.Td([str(number_of_sessions)])
                    ])
                ], className='table table is-narrow is-bordered is-fullwidth')
            ]),
            html.Div([
                html.Table(
                    # Header
                    [html.Tr([html.Th(col, className='thead', style={'fontSize': '0.7em'}) for col in df_cfc_local.columns])] +

                    # Body
                    [html.Tbody(
                        tr_list, className='tbody'
                    )],
                    className='table table is-narrow is-striped is-fullwidth is-bordered'
                )
            ])
        ])

import numpy as np
import pandas as pd
from datetime import datetime


def load_data():
    dfa = pd.read_csv('../data/source/apps.csv')
    dfo = pd.read_csv('../data/source/orders.csv')
    dfl = pd.read_csv('../data/source/link_data.csv')
    return dfa, dfo, dfl


def clean_data(dfa, dfo, dfl):
    # Reformat time to follow one convention
    dfo['Time'] = pd.to_datetime(dfo['Time'])
    dfa['StartTime'] = pd.to_datetime(dfa['StartTime'])
    dfa['EndTime'] = pd.to_datetime(dfa['EndTime'])

    # Rename columns to follow one convention
    dfa = dfa.rename(index=str, columns={'AppId': 'ApplicationName', 'Id': 'ApplicationSessionId'})
    dfl = dfl.rename(index=str, columns={
                     'ApplicationID': 'ApplicationSessionId', 'SessionID': 'SessionId', 'PlaceID': 'PlaceId'})

    # All ids to uppercase to allow indexing
    dfl['SessionId'] = dfl['SessionId'].str.upper()
    dfo['SessionId'] = dfo['SessionId'].str.upper()
    dfa['ApplicationSessionId'] = dfa['ApplicationSessionId'].str.upper()
    dfl['ApplicationSessionId'] = dfl['ApplicationSessionId'].str.upper()

    dfa = dfa.drop_duplicates()
    dfo = dfo.drop_duplicates()
    dfl = dfl.drop_duplicates()

    dfa = dfa.set_index('ApplicationSessionId')
    dfl = dfl.set_index('ApplicationSessionId')

    return dfa, dfo, dfl


def create_app_usage_dataframe():
    """
    Creates dataframe of app usage in minutes, number of orders and cumulative revenue, grouped by session.
    """
    dfa, dfo, dfl = load_data()
    dfa, dfo, dfl = clean_data(dfa, dfo, dfl)

    app_names = dfl['ApplicationName'].unique()
    cols = np.concatenate((np.array(['SessionId', 'Orders', 'Revenue', 'DateTime']), app_names))

    dfc = pd.DataFrame(columns=cols)
    dft = pd.DataFrame(columns=cols)
    dfc = dfc.set_index('SessionId')
    dft = dft.set_index('SessionId')
    dfo_np = np.array(dfo)

    for row in dfo_np:
        order_id = row[0]
        revenue = row[1]
        session_id = row[2]
        order_time = row[3]

        if not (dfc.index == session_id).any():
            dfc.loc[session_id] = 0

        if not (dft.index == session_id).any():
            dft.loc[session_id] = 0

        dfc_row = dfc.loc[session_id]
        dft_row = dft.loc[session_id]

        dfc_row['Revenue'] += revenue
        dfc_row['Orders'] += 1
        dfc_row['DateTime'] = order_time

        dft_row[['Revenue', 'Orders', 'DateTime']] = dfc_row[['Revenue', 'Orders', 'DateTime']]

        all_links = dfl.loc[dfl['SessionId'] == session_id]

        for link_row in all_links.itertuples():
            link_application_session_id = link_row[0]

            if (dfa.index == link_application_session_id).any():
                app_row = dfa.loc[link_application_session_id]
                app_name = app_row['ApplicationName']
                app_start_time = app_row['StartTime']
                app_end_time = app_row['EndTime']
                app_diff_time = (app_end_time - app_start_time).total_seconds()
                dfc[app_name][session_id] += (app_diff_time / 60)
                dft[app_name][session_id] += 1

    dfc.to_csv('../data/processed/usage.csv')
    dft.to_csv('../data/processed/starts.csv')


if __name__ == '__main__':
    create_app_usage_dataframe()

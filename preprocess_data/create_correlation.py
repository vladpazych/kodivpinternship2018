import numpy as np
import pandas as pd
from scipy import stats


def create_correlation_dataframe():
    df = pd.read_csv('../data/processed/usage.csv')
    df['DateTime'] = pd.to_datetime(df['DateTime'])

    app_names = df.columns[4:-1]

    normal_cols = np.concatenate((['Revenue'], app_names))

    # Ranking for calculating Spearnam's coefficient.
    # df[app_names] = df[app_names].rank(axis=1, method='dense')

    dft_cols = np.concatenate((['Hour'], app_names))
    dft = pd.DataFrame(columns=dft_cols)
    dft = dft.set_index('Hour')

    hours = np.arange(0, 24)

    for hour in hours:
        rows = df.loc[df['DateTime'].dt.hour == hour]

        if not (dft.index == hour).any():
            dft.loc[hour] = np.nan

        if (rows.shape[0] == 0):
            continue

        for app_name in app_names:
            revenue = rows['Revenue']
            r = rows[app_name].corr(revenue, min_periods=10, method='pearson')
            dft[app_name][hour] = r

    dft = dft.fillna(dft.mean())
    dft = dft.round(decimals=3)
    dft.to_csv('../data/processed/correlation.csv')


if __name__ == '__main__':
    create_correlation_dataframe()

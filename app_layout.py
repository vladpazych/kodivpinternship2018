import numpy as np
import pandas as pd
import dash
import plotly.graph_objs as go
import dash_core_components as dcc
import dash_html_components as html


def create(app: dash.Dash):
    app.layout = html.Div(children=[
        html.Div([
            html.H1('Kodisoft internship 2018', className='title is-1'),
            html.H3(['Vladislav Pazych | ', html.A('vladpazych@gmail.com', href='mailto:vladpazych@gmail.com'), ' | +380 (96) 375-81-70'],
                    className='subtitle is-5'),
        ], style={'margin': '3em 0'}),

        html.Div([
            html.Div([
                html.Div([
                    html.Div([
                        dcc.RadioItems(options=[
                            {'label': 'Single hour', 'value': 'DisableTimeRange'},
                            {'label': 'Time range', 'value': 'EnableTimeRange'}
                        ], value='DisableTimeRange', id='time-range-selector', className='control radio-group', style={'fontSize': '0.8em', 'fontStyle': 'italic', 'position': 'absolute', 'top': 0, 'left': 0}),
                        html.P('Start and End hours are inclusive.', style={'fontSize': '0.8em', 'fontStyle': 'italic', 'textAlign': 'right', 'position': 'absolute', 'top': 0, 'right': 0}),
                        html.Div(id='hour-slider-container', style={'margin': '1em 0 4em 0', 'paddingTop': '2em'})
                    ], style={'position': 'relative'}),
                    html.Div(id='correlation-table'),
                    html.P('Sorting is based on correlation coefficients between time spent in apps and total revenue, grouped by session id. Time periods 23:00 - 08:00, inclusive, did not have enough data to calculate correlation coefficients, therefore coefficients for these periods were calculated as avarage during the day.', style={'fontSize': '0.8em', 'fontStyle': 'italic'}),
                    html.P('', style={'fontSize': '0.8em', 'fontStyle': 'italic'}),
                ], style={'margin': '2em 4em 0 0'}),
                dcc.Graph(
                    style={'height': '100%', 'width': 'auto', 'margin': '1em'},
                    id='application-scatter'
                )
            ], style={'display': 'grid', 'gridTemplateColumns': '40% 60%', 'minHeight': '40em', 'margin': '2em 0'}),
        ])
    ], style={'width': '80vw', 'margin': ' 0 auto 10em auto', 'minHeight': '100vh'})

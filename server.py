import dash
import os
import app_layout
import app_callbacks
import app_style
import flask


# Allow static folder to be served by flask
STATIC_PATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'static')

# Init app
server = flask.Flask(__name__)
app = dash.Dash(__name__, server=server)

# We have callbacks linked to output of other callbacks, this raises warning.
# We do it consciously, so disabling warning:
app.config['suppress_callback_exceptions'] = True


app_layout.create(app)
app_style.create(app)
app_callbacks.create(app)


if __name__ == '__main__':
    app.run_server(debug=True)

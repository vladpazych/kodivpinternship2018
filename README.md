Live preview available at [kodivpinternship2018.herokuapp.com](https://kodivpinternship2018.herokuapp.com/)

# How to run locally

### 1. Download source code
```
git clone https://gitlab.com/vladpazych/kodivpinternship2018.git
```

Or download [`.zip` archive](https://gitlab.com/vladpazych/kodivpinternship2018/-/archive/master/kodivpinternship2018-master.zip).

### 2. If you don't have `pipenv`
```
pip install pipenv
```

### 3. Install dependencies
```
cd kodivpinternship2018
pipenv install
```

### 4. Run server
```
pipenv shell
python ./server.py
```

### 5. Open [localhost:8050](http://localhost:8050/)
import pandas as pd
import dash
from lib import generate_color

df_cfc = pd.read_csv('./data/processed/correlation.csv')
app_names = df_cfc.columns[1:]
app_colors = {}
colors = ['#4527A0', '#1565C0', '#00838F', '#558B2F', '#FF8F00', '#D84315', '#37474F', '#c62828', '#AA00FF']

for app_name in app_names:
    app_colors[app_name] = colors.pop()


def create(app: dash.Dash):
    app.css.append_css({
        'external_url': '/static/main.css',
    })

    app.css.append_css({
        'external_url': '/static/custom.css',
    })
